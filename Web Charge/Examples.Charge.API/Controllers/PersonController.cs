﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Examples.Charge.Application.Dtos;
using Examples.Charge.Application.Interfaces;
using Examples.Charge.Application.Messages.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Examples.Charge.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : BaseController
    {
        private IPersonFacade _facade;

        public PersonController(IPersonFacade facade)
        {
            _facade = facade;
        }

        [HttpGet]
        public async Task<ActionResult<PersonResponse>> Get()
        {
            return Response(await _facade.FindAllAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PersonResponse>> Get(int id)
        {
            return Response(await _facade.FindById(id));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PersonDto person)
        {
            var resp = await _facade.Add(person);
            return Response(resp.PersonObjects.FirstOrDefault()?.BusinessEntityID, resp.PersonObjects);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] PersonDto person)
        {
            var resp = await _facade.Update(person);
            return Response(resp);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var resp = await _facade.Remove(id);
            return Response(resp);
        }
    }
}
