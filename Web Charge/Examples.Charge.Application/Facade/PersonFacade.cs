﻿using AutoMapper;
using Examples.Charge.Application.Dtos;
using Examples.Charge.Application.Interfaces;
using Examples.Charge.Application.Messages.Response;
using Examples.Charge.Domain.Aggregates.PersonAggregate;
using Examples.Charge.Domain.Aggregates.PersonAggregate.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examples.Charge.Application.Facade
{
    public class PersonFacade : IPersonFacade
    {
        private readonly IPersonService _personService;
        private readonly IMapper _mapper;

        public PersonFacade(IPersonService personService, IMapper mapper)
        {
            _personService = personService;
            _mapper = mapper;
        }

        public async Task<PersonResponse> FindAllAsync()
        {
            var result = await _personService.FindAllAsync();
            var resp = new PersonResponse();
            resp.PersonObjects = new List<PersonDto>();
            resp.PersonObjects.AddRange(result.Select(x => _mapper.Map<PersonDto>(x)));
            return resp;
        }

        public async Task<PersonResponse> FindById(int id)
        {
            var model = await _personService.FindAsync(id);
            return new PersonResponse
            {
                PersonObjects = new List<PersonDto> { _mapper.Map<PersonDto>(model) }
            };
        }

        public async Task<PersonResponse> Add(PersonDto personDto)
        {
            var model = _mapper.Map<Person>(personDto);
            await _personService.Add(model);

            return new PersonResponse
            {
                PersonObjects = new List<PersonDto> { _mapper.Map<PersonDto>(model) }
            };
        }

        public async Task<PersonResponse> Update(PersonDto personDto)
        {
            var model = _mapper.Map<Person>(personDto);
            await _personService.Update(model);
            return new PersonResponse()
            {
                PersonObjects = new List<PersonDto>() { personDto }
            };
        }

        public async Task<PersonResponse> Remove(int id)
        {
            await _personService.Remove(id);
            return new PersonResponse();
        }

    }
}
