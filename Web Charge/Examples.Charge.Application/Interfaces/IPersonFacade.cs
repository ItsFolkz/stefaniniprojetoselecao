﻿using Examples.Charge.Application.Dtos;
using Examples.Charge.Application.Messages.Response;
using System.Threading.Tasks;

namespace Examples.Charge.Application.Interfaces
{
    public interface IPersonFacade
    {
        Task<PersonResponse> FindAllAsync();
        Task<PersonResponse> FindById(int id);
        Task<PersonResponse> Remove(int id);
        Task<PersonResponse> Add(PersonDto personDto);
        Task<PersonResponse> Update(PersonDto personDto);
    }
}