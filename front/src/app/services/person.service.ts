import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { urlApi } from 'src/environments/environment';
import { Person } from '../models/person';
import { ResponseApi } from '../models/response';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  private controllerName = "Person"

  constructor(private httpClient: HttpClient) {}

  GetAll(){
    return this.httpClient.get<ResponseApi<Person[]>>(`${urlApi}${this.controllerName}`).toPromise()
  }

  Update(person: Person){

    return new Promise<ResponseApi<Person[]>>(res => {
      this.RemoveById(person.businessEntityID)
      .then(() => {
        this.Add(person).then(data => res(data));
      })

    });

   
  }

  Add(person: Person){
    return this.httpClient.post<ResponseApi<Person[]>>(`${urlApi}${this.controllerName}`, person).toPromise()
  }

  RemoveById(id: number)
  {
    return this.httpClient.delete<void>(`${urlApi}${this.controllerName}/${id}`).toPromise()
  }

}
